# php-examples

Php examples of various things.

## Set Up

After cloning or downloading, files are run with php, unless otherwise stated.

```
php "filename.php" # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

