<?php
if (true)
{
	echo "This will show\n";
}

if (false)
{
	echo "This won't show";
}
else
{
	echo "This will show\n";
}

if (false)
{
	echo "This won't show";
}
else if(true)
{
	echo "This will show\n";
}
else
{
	echo "This won't show";
}


if (true and true)
{
	echo "This will show\n";
}

if (true and false)
{
	echo "This won't show";
}

if (false and false)
{
	echo "This won't show";
}

if (true or true)
{
	echo "This will show\n";
}

if (true or false)
{
	echo "This will show\n";
}

if (false or false)
{
	echo "This won't show";
}

$x = 36;
$y = 12;
if ($x == $y)
{
	echo "x equals y\n";
}

if ($x != $y)
{
	echo "x doesn't equal y\n";
}

if ($x > $y)
{
	echo "x greater than y\n";
}

if ($x < $y)
{
	echo "x less than y";
}
?>
