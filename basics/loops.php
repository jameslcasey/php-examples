<?php
$running = true;
$num = 5;
while ($running)
{
	$num -= 1;
	if ($num < 1)
	{
		echo "Exiting while loop";
		$running = false;
	}
}

for ($i = 0; $i < 13; $i++)
{
	echo "\n5 * $i = ", 5 * $i;
}

echo "\n";

$words = ["hello", "word", "some", "thing"];
foreach ($words as $word)
{
	echo $word, "\n";
}
?>
